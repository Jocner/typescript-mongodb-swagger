"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.allAvatar = exports.newAvatar = void 0;
const avatar_1 = require("../services/avatar");
// export class AvatarController {
//     constructor(private readonly avatarServices: avatarService) {}
const newAvatar = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const avatar = yield avatar_1.register(req.body);
        const result = avatar ? avatar : 'Fail Service';
        return res.json(result);
    }
    catch (err) {
        console.log(`Fail Service ${err}`);
    }
});
exports.newAvatar = newAvatar;
const allAvatar = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const avatar = yield avatar_1.all();
        return res.json(avatar);
    }
    catch (err) {
        console.log(`Fail Service ${err}`);
    }
});
exports.allAvatar = allAvatar;
// }
