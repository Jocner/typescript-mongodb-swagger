"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const avatarSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    imageUrl: {
        type: String,
        required: false,
    },
    image: {
        type: String,
        required: false,
    },
    status: {
        type: String,
        require: false,
        enum: ['enabled', 'preview', 'disabled', 'deleted'],
        default: 'disabled',
    },
});
// export interface IUser extends Document {
//     name: string;
//     createdAt: Date;
//     imageUrl: string;
//     status: string;
// }
// export default model<IUser>('Avatar', avatarSchema);
exports.default = mongoose_1.model('Avatar', avatarSchema);
