"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const avatar_controller_1 = require("../controllers/avatar.controller");
const router = express_1.Router();
router.post('/avatar', avatar_controller_1.newAvatar);
router.get('/avatar', avatar_controller_1.allAvatar);
exports.default = router;
